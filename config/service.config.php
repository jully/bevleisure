<?php

return [
    'apiService' => [
        'protocol' => 'https',
        'host' => 'hcms-dev.mekongleisuretravel.com/be/api/v1',
        'path' => [
            'auth' => 'getAuth/',
            'hotelInfo' => 'hotelinfo/',
            'roomTypeInfo' => 'roomtypeinfo/',
            'search' => 'search/',
            'detail' => 'detail/',
            'createBooking' => 'book/',
            'updateBooking' => 'update/',
            'retriveBooking' => 'retrivebook/',
        ]
    ],
    'authService' => [
        'protocol' => 'https',
        'host' => 'balancer-node.mekongleisuretravel.com/services',
        'path' => [
            'auth' => 'cache/app/index/pob-apic/',
        ]
    ]
];
