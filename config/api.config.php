<?php

return [
    'dataService' => [
        'protocol' => 'https',
        'host' => '116ws.mekongleisuretravel.com',
        'path' => [
            'searchAll' => 'services/all-search/',
        ]
    ],
    'apiService' => [
        'protocol' => 'https',
        'host' => 'http://hotels-dev.mekongleisuretravel.com/ihs/v2.3',
        'path' => [
            'searchRoom' => '',
        ]
    ]
];
