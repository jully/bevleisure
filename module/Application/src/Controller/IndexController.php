<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */
declare(strict_types=1);

namespace Application\Controller;

use Laminas\View\Model\ViewModel;
use Core\Vls\VlsAjax;
use Core\Vls\VlsHelper;
use Laminas\Json\Json;
use Laminas\Mvc\Controller\Plugin\Redirect;
use Laminas\View\Model\JsonModel;

class IndexController extends VlsController {

  public function construct($container) {
    parent::__construct($container);
  }

  public function indexAction() {
    $hotelPlug = $this->params()->fromRoute('hotelPlug', '');
    
    $view = new ViewModel();

    if (empty($hotelPlug)) {
      $view->layout('layout/layout_error');
      return $view;
    }

    $params = [
        'method' => 'POST',
        'data' => ['hotelplug' => $hotelPlug]
    ];

    $auth = callAPI($this->apiHost, $this->configService['apiService']['path']['auth'], $params);
    
    if ($auth['status'] !== 'SUCCESS') {
      $view->layout('layout/layout_error');
      return $view;
    }

    $this->session->token = $auth['result'];

    $params = [
        'method' => 'GET',
        'token' => $this->session->token,
        'data' => []
    ];

    $hotelInfo = callAPI($this->apiHost, $this->configService['apiService']['path']['hotelInfo'], $params);
    $this->session->hotelInfo = $hotelInfo['result'];
    $this->session->hotelInfo->hotelKey = $hotelPlug;
    return $view;
  }

  public function getRoomsAction() {
    $farriveDate = $this->params()->fromPost('arrive', '');
    $fdepartureDate = $this->params()->fromPost('departure', '');
    $frooms = $this->params()->fromPost('rooms', []);
    $sort = $this->params()->fromPost('sort');

    if (empty($farriveDate)) {
      VlsAjax::setMessage('Arrive is required.');
      VlsAjax::outData(false);
    }
    if (empty($fdepartureDate)) {
      VlsAjax::setMessage('Departure is required.');
      VlsAjax::outData(false);
    }

    $rooms = [];
    foreach ($frooms as $key => $roomInfo) {
      $arr = [
          "adt" => $roomInfo['adt'],
          "chd" => [],
      ];
      for ($i = 0; $i < $roomInfo['chd']; $i++) {
        $arr["chd"][] = 6;
      }
      $rooms[] = $arr;
    }

    $data = [
        'arrivalDate' => date('Y-m-d', strtotime($farriveDate)),
        'departureDate' => date('Y-m-d', strtotime($fdepartureDate)),
        'rooms' => $rooms
    ];
    
    if (in_array($sort['field'], ['rate'])) {
      $data['order'] = ["{$sort['field']} {$sort['type']}"];
    }
    
    $params = [
        'method' => 'POST',
        'token' => $this->session->token,
        'data' => $data
    ];


    $results = callAPI($this->apiHost, $this->configService['apiService']['path']['search'], $params);

    if ($results['status'] != 'SUCCESS') {
      VlsAjax::setMessage('Opps, there is something wrong happened.');
      VlsAjax::outData(false);
    }

    $roomRates = (!empty($results['result'])) ? $results['result']->roomRates : [];

    $requestInfo = urlencode(base64_encode(Json::encode($data)));

    //Render html
    $view = new ViewModel(['rooms' => $roomRates, 'requestInfo' => $requestInfo]);
    $view->setTemplate('application/index/room-list');
    $html = $this->renderer->render($view);

    VlsAjax::setHtml('listRooms', $html);
    VlsAjax::outData();
  }

  public function getRoomInfoAction() {
    $roomTypeCode = $this->params()->fromPost('roomTypeCode');

    if (empty($roomTypeCode)) {
      VlsAjax::setMessage('Invalid Room Info.');
      VlsAjax::outData(false);
    }

    $params = [
        'method' => 'GET',
        'token' => $this->session->token,
        'data' => [
            'roomTypeCode' => $roomTypeCode
        ]
    ];

    $results = callAPI($this->apiHost, $this->configService['apiService']['path']['roomTypeInfo'], $params);

    if ($results['status'] != 'SUCCESS') {
      VlsAjax::setMessage('This room type is not existed.');
      VlsAjax::outData(false);
    }

    $view = new ViewModel(['roomInfo' => $results['result']]);
    $view->setTemplate('application/index/modal-room-info');
    $html = $this->renderer->render($view);

    VlsAjax::setHtml('commonDialog', $html);
    VlsAjax::outData();
  }

  public function reservationFormAction() {
    $code = $this->params()->fromQuery('code');
    $roomTypeCode = $this->params()->fromQuery('rT', 'test');
    $data = Json::decode(base64_decode(urldecode($code)));

    if (!isset($data->arrivalDate) || !isset($data->departureDate) || !isset($data->rooms) || empty($roomTypeCode)) {
      VlsAjax::setMessage('Invalid room info.');
      VlsAjax::outData(false);
    }

    $data->roomTypeCode = $roomTypeCode;

    $params = [
        'method' => 'POST',
        'token' => $this->session->token,
        'data' => $data
    ];

    $results = callAPI($this->apiHost, $this->configService['apiService']['path']['detail'], $params);

    if ($results['status'] != 'SUCCESS') {
      VlsAjax::setMessage('Opps, there is something wrong happened.');
      VlsAjax::outData(false);
    }

    $view = new ViewModel(['bookingInfo' => $data, 'roomInfo' => $results['result']->roomRates]);
    $view->setTemplate('application/index/guest-form');
    return $view;
  }

  public function searchBookingAction() {
    $view = new ViewModel();
    $view->setTemplate('application/index/modal-search-booking');
    $html = $this->renderer->render($view);

    VlsAjax::setHtml('commonDialog', $html);
    VlsAjax::outData();
  }

  public function retriveBookingAction() {
    $bookingCode = $this->params()->fromPost('bookingCode');
    $email = $this->params()->fromPost('email');

    if (empty($bookingCode) && empty($email)) {
      VlsAjax::setMessage('Invalid booking.');
      VlsAjax::outData(false);
    }

    $data = [
        'code' => $bookingCode,
        'email' => $email
    ];

    $params = [
        'method' => 'POST',
        'token' => $this->session->token,
        'data' => $data
    ];

    //Retrive Booking
    $retrivedBooking = callAPI($this->apiHost, $this->configService['apiService']['path']['retriveBooking'], $params);

    if ($retrivedBooking['status'] !== 'SUCCESS') {
      VlsAjax::setMessage('Booking not found.');
      VlsAjax::outData(false);
    }

    $responseInfo = urlencode(base64_encode(Json::encode($retrivedBooking['result'])));

    VlsAjax::outData($responseInfo);
  }

  public function myBookingFormAction() {
    $code = $this->params()->fromQuery('code');
    $data = Json::decode(base64_decode(urldecode($code)));

    if (!isset($data->bookingCode)) {
      VlsAjax::setMessage('Invalid booking info.');
      VlsAjax::outData(false);
    }

    $view = new ViewModel(['myBooking' => $data]);
    $view->setTemplate('application/index/my-bookings');
    return $view;
  }

  public function reservationAction() {
    $payMethod = $this->params()->fromRoute('type', '');

    if (!empty($payMethod) && $payMethod == 'response') {
      $params = $this->params()->fromQuery();
      $pr = $this->payment($params, 'response');

      if ($pr->status != 'SUCCESS') {
        VlsAjax::setMessage('Transaction failed.');
        VlsAjax::outData(false);
      }

      $params['mode'] = 'TEST';
      $data = [
          'id' => str_replace('BK_', '', $params['vpc_OrderInfo']),
          'status' => 'SUCCESS',
          'dataPay' => Json::encode($params)
      ];

      $params = [
          'method' => 'POST',
          'token' => $this->session->token,
          'data' => $data,
      ];
      
      //Update booking
      $updatedBooking = callAPI($this->apiHost, $this->configService['apiService']['path']['updateBooking'], $params);
      
      if ($updatedBooking['status'] !== 'SUCCESS') {
        VlsAjax::setMessage('Booking can not confirmed.');
        VlsAjax::outData(false);
      }

      $data = [
          'code' => $updatedBooking['result']->bookingCode,
          'email' => $updatedBooking['result']->booker->email
      ];

      $params = [
          'method' => 'POST',
          'token' => $this->session->token,
          'data' => $data
      ];

      //Retrive Booking
      $retrivedBooking = callAPI($this->apiHost, $this->configService['apiService']['path']['retriveBooking'], $params);

      if ($retrivedBooking['status'] !== 'SUCCESS') {
        VlsAjax::setMessage('Booking not found.');
        VlsAjax::outData(false);
      }

      $view = new ViewModel(['myBooking' => $retrivedBooking['result']]);
      $view->setTemplate('application/index/my-bookings');
      return $view;
    }

    $rooms = $this->params()->fromPost('rooms', []);
    $booker = $this->params()->fromPost('booker', []);
    $notes = $this->params()->fromPost('notes', '');
    $token = $this->params()->fromPost('tk', '');

    if (empty($rooms) || empty($booker) || empty($token)) {
      VlsAjax::setMessage('Thông tin không hợp lệ.');
      VlsAjax::outData(false);
    }

    $data = [
        'booker' => $booker,
        'rooms' => $rooms,
        'notes' => $notes,
        'token' => $token,
    ];

    $params = [
        'method' => 'POST',
        'token' => $this->session->token,
        'headers' => ['User-Agent' => $_SERVER['HTTP_USER_AGENT'], 'Ip' => $_SERVER['REMOTE_ADDR']],
        'data' => $data,
    ];
    
    //Create booking
    $createdBooking = callAPI($this->apiHost, $this->configService['apiService']['path']['createBooking'], $params);
    
    if ($createdBooking['status'] !== 'SUCCESS') {
      VlsAjax::setMessage('Your booking cant not be created.');
      VlsAjax::outData(false);
    }
    
    $paymentParams = [
        'amount' => $createdBooking['result']->total_amount,
        'currency' => $createdBooking['result']->currency,
        'bookingId' => $createdBooking['result']->id,
    ];

    $pay = $this->payment($paymentParams);
    VlsAjax::outData($pay);
  }

  private function payment($params, $type = 'request') {
    $onepay = \Core\Service\Onepay::getInstance();
    $response = new \stdClass();

    $transType = 'LOCAL';

    if ($type == 'response') {
      $response->status = 'FAIL';
      $response->message = 'Transaction fail';

      $pay = $onepay->response($params);

      if (is_null($pay)) {
        return $response;
      }
      $response->status = $pay['status'];

      return $response;
    }

    $returnUrl = "http://".$_SERVER['HTTP_HOST'].'/reservation-onepay-response';

    $amount = $params['amount'];
    $title = 'BK_'.$params['bookingId'];
    $bookingId = $params['bookingId'];
    $transType = 'LOCAL';
//    $merchtxnRef = rand(5,15);
    $merchtxnRef = '';

    switch ($transType) {
      case 'LOCAL':
        $returnUrl = $onepay->request($_SERVER['REMOTE_ADDR'], $title, $amount, $returnUrl, $merchtxnRef, 'VND', 'vn', $bookingId, 'domestic');
        break;
      case 'GLOBAL':
        $returnUrl = $onepay->request($_SERVER['REMOTE_ADDR'], $title, $amount, $returnUrl, $merchtxnRef, 'VND', 'en', $bookingId, 'international');
        exit;
        break;
    }

    return $returnUrl;
  }

  public function getCountriesAction() {
    $request = $this->getRequest();
    $reqData = $request->getQuery()->toArray();

    if ($request->isPost()) {
      $reqData = Json::decode($request->getContent(), Json::TYPE_ARRAY);
    }
    $keyword = $reqData['query'];

    $countries = VlsHelper::getCountries($keyword);
    
    return new JsonModel(['items' => $countries, 'total' => count($countries)]);
  }

}
