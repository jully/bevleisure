<?php

namespace Application\Controller;

use Laminas\Mvc\Controller\AbstractActionController;

class VlsController extends AbstractActionController
{
  protected $container;
  protected $session;
  protected $renderer;
  protected $configService;
  protected $apiHost;
  protected $authHost;

  public function __construct($container)
  {
    $this->container = $container;
    $this->renderer = $container->get('Laminas\View\Renderer\PhpRenderer');
    $this->configService = \Laminas\Config\Factory::fromFile(ROOT_DIR . '/config/service.config.php');
    $this->apiHost = sprintf('%s://%s', $this->configService['apiService']['protocol'], $this->configService['apiService']['host']);
    $this->authHost = sprintf('%s://%s', $this->configService['authService']['protocol'], $this->configService['authService']['host']);
    $this->session = $container->get('BeSessionContainer');

  }
}
