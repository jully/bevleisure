<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */
declare(strict_types=1);

namespace Application;
use Laminas\Mvc\MvcEvent;
use Laminas\Session\SessionManager;

class Module {

  public function getConfig(): array {
    return include __DIR__ . '/../config/module.config.php';
  }

  public function onBootstrap(MvcEvent $event) {
    $application = $event->getApplication();
    $serviceManager = $application->getServiceManager();
    $eventManager = $application->getEventManager();

    // The following line instantiates the SessionManager and automatically
    // makes the SessionManager the 'default' one.
//    $sessionManager = $serviceManager->get(SessionManager::class);
//    $this->forgetInvalidSession($sessionManager);

    $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'onDispatchError'], 100);
    $eventManager->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 200);
  }

  public function onDispatchError(MvcEvent $e) {
    $viewModel = $e->getViewModel();
    $viewModel->setTemplate('layout/layout_error');
  }

  public function onDispatch(MvcEvent $e) {
    $app = $e->getApplication();
    $sm = $app->getServiceManager();
    $sessionContainer = $sm->get('BeSessionContainer');
    
    $viewModel = $app->getMvcEvent()->getViewModel();
    $viewModel->hotelInfo = $sessionContainer->hotelInfo;
  }

}
