<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */

declare(strict_types=1);

namespace Application;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'booking' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/booking[/:hotelPlug]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'detailRoom' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/detail-room',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'detailRoom',
                    ],
                ],
            ],
            'reservationForm' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/reservation-form',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'reservationForm',
                    ],
                ],
            ],
            'onePay' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/reservation-onepay-response',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'reservation',
                        'type'     => 'response',
                    ],
                ],
            ],
            'myBooking' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/my-booking',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'myBookingForm',
                    ],
                ],
            ],
            'getCountry' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/get-country',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'getCountries',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => Controller\Factory\IndexControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helpers' => [
      'factories' => [
        View\HelperView::class => View\Factory\HelperViewFactory::class,
      ],
        
      'aliases' => [
        'config' => View\HelperView::class
      ]
    ],
    'session_containers' => [
        'BeSessionContainer'
    ],
];
