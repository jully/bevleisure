<?php

namespace Core\Service;

class Onepay {

//  const DO_SERVICE_URL = 'https://onepay.vn/onecomm-pay/vpc.op';
//  const IN_SERVICE_URL = 'https://onepay.vn/vpcpay/vpcpay.op';
//  const DO_SECURE_SECRET = '8591014E884D468656D1ED288C2437D8';
//  const IN_SECURE_SECRET = '730EE37DE3975A5B134373AB08C2C03B';
//  const DO_ACCESSCODE = 'FG4QAIJ2';
//  const IN_ACCESSCODE = '0BCD4E61';
//  const DO_MERCHANTCODE = 'MKLEISURE';
//  const IN_MERCHANTCODE = 'MKLEISURE';

  /* Test */
    const DO_SERVICE_URL = 'https://mtf.onepay.vn/onecomm-pay/vpc.op';
    const IN_SERVICE_URL = 'https://mtf.onepay.vn/vpcpay/vpcpay.op';
    const DO_SECURE_SECRET = 'A3EFDFABA8653DF2342E8DAC29B51AF0';
    const IN_SECURE_SECRET = '6D0870CDE5F24F34F3915FB0045120DB';

    const DO_ACCESSCODE = 'D67342C2';
    const IN_ACCESSCODE = '6BEB2546';
    const DO_MERCHANTCODE = 'ONEPAY';
    const IN_MERCHANTCODE = 'TESTONEPAY';
   

  protected $_params = array(
      'Title' => 'VPC 3-Party',
      'vpc_Merchant' => 'MKLEISURE',
//      'vpc_AccessCode' => '0BCD4E61',
      'vpc_AccessCode' => 'D67342C2',
      'vpc_MerchTxnRef' => '',
      'vpc_OrderInfo' => 'JSECURETEST01',
      'vpc_Amount' => 100,
      'vpc_ReturnURL' => '',
      'vpc_Version' => 2,
      'vpc_Command' => 'pay',
      'vpc_Locale' => 'vn',
      'vpc_TicketNo' => '',
//      'AgainLink' => 'http://be.vleisure.tech/',
      'AgainLink' => 'http://be.tech/reservation-onepay-response',
  );

  /**
   * @var $_instance the unique instance of cache storage
   */
  private static $__instance = null;

  public function __construct() {
    return $this;
  }

  public static function getInstance() {
    if (null === self::$__instance) {
      $thisClass = __CLASS__;
      self::$__instance = new $thisClass();
    }
    return self::$__instance;
  }

  public function request($ipClient, $orderTitle, $amount, $returnUrl, $merchTxnRef = '', $currencyCode = 'VND', $localeCode = 'vn', $bookingId = Null, $type = 'international') {
    $this->_params['vpc_Merchant'] = self::IN_MERCHANTCODE;
    $this->_params['vpc_AccessCode'] = self::IN_ACCESSCODE;
    $this->_params['vpc_MerchTxnRef'] = ($merchTxnRef) ? $merchTxnRef : (date('YmdHis') . rand());
    $this->_params['vpc_TicketNo'] = $ipClient;
    $this->_params['vpc_Locale'] = $localeCode;
    $this->_params['vpc_Amount'] = (ceil($amount) * 100);
    $this->_params['vpc_OrderInfo'] = $orderTitle;
    $this->_params['vpc_ReturnURL'] = $returnUrl;
    $keySecureSecret = self::IN_SECURE_SECRET;
    $serviceUrl = self::IN_SERVICE_URL;

    if ($type == 'domestic') {
      $this->_params['vpc_Merchant'] = self::DO_MERCHANTCODE;
      $this->_params['vpc_AccessCode'] = self::DO_ACCESSCODE;
      $this->_params['vpc_Currency'] = 'VND';
      $keySecureSecret = self::DO_SECURE_SECRET;
      $serviceUrl = self::DO_SERVICE_URL;
    }
    
    $strHashData = '';
    ksort($this->_params);
    foreach ($this->_params as $key => $value) {
      if ((strlen($value) > 0) && ((substr($key, 0, 4) == 'vpc_') || (substr($key, 0, 5) == 'user_'))) {
        $strHashData .= $key . '=' . $value . '&';
      }
    }

    $strHashData = rtrim($strHashData, '&');
    $query = http_build_query($this->_params) . '&vpc_SecureHash=' . self::hashHmac($strHashData, $keySecureSecret);
    
    return $serviceUrl. '?' . $query;   
  }

  public function response($params, $type = 'domestic') {

    $keySecureSecret = self::IN_SECURE_SECRET;

    if ($type == 'domestic') {
      $keySecureSecret = self::DO_SECURE_SECRET;
    }

    $response = array('secure_hash' => '', 'response_code' => $params['vpc_TxnResponseCode'], 'status' => 'UNSPECIFIED', 'message' => 'Unspecified Failure');
    // $hashValidated = 'INVALID HASH';
    $secureHash = $params['vpc_SecureHash'];
    unset($params['vpc_SecureHash']);
    // set a flag to indicate if hash has been validated
    $errorExists = false;
    ksort($params);
    if ($params['vpc_TxnResponseCode'] == '7') {
      return $response;
    }

    $strHashData = '';
    foreach ($params as $key => $value) {
      if ($key != 'vpc_SecureHash' && (strlen($value) > 0) && ((substr($key, 0, 4) == 'vpc_') || (substr($key, 0, 5) == 'user_'))) {
        $strHashData .= $key . '=' . $value . '&';
      }
    }

    $strHashData = rtrim($strHashData, '&');
    if (strtoupper($secureHash) == self::hashHmac($strHashData, $keySecureSecret)) {
      $response['secure_hash'] = 'CORRECT';
    }

    if ($response['secure_hash'] != 'CORRECT') {
      $response['status'] = 'SECURE_INCORRECT';
      $response['message'] = 'Secure hash Failure';
      return $response;
    }
    
    if ($type == 'domestic') {
      $result = $this->domesticsResult($params['vpc_TxnResponseCode']);
    } else {
      $result = $this->internationalResult($params['vpc_TxnResponseCode']);
    }
    
    $response['status'] = $result['status'];
    $response['message'] = $result['message'];
    return $response;
  }
  
  public function domesticsResult($code) {
    $response = [];
    switch ($code) {
      case '0'://Approved//Giao dịch thành công
        $response['status'] = 'SUCCESS';
        $response['message'] = 'Successful transaction.';
        break;
      case '1'://Bank Declined Transaction//Giao dịch không thành công, Ngân hàng từ chối giao dịch
        $response['status'] = 'DECLINED';
        $response['message'] = 'Bank Declined Transaction.';
        break;
      case '3'://Merchant is not exist//Giao dịch không thành công, Mã đơn vị không tồn tại
        $response['status'] = 'MERCHANT_NOT_EXIST';
        $response['message'] = 'Merchant is not exist.';
        break;
      case '4'://Invalid access code//Giao dịch không thành công, Không đúng access code
        $response['status'] = 'ACCESS_CODE_INVALID';
        $response['message'] = 'Access code invalid.';
        break;
      case '5'://Invalid amount//Giao dịch không thành công, Số tiền không hợp lệ
        $response['status'] = 'AMOUNT_INVALID';
        $response['message'] = 'Amount invalid.';
        break;
      case '6'://Invalid currency code//Giao dịch không thành công, Mã tiền tệ không tồn tại
        $response['status'] = 'CURRENCY_INVALID';
        $response['message'] = 'Currency invalid.';
        break;
      case '7'://Unspecified Failure//Giao dịch không thành công, Lỗi không xác định
        $response['status'] = 'UNSPECIFIED';
        $response['message'] = 'Unspecified Failure';
        break;
      case '8':// Invalid Card number// Giao dịch không thành công, Số thẻ không đúng
        $response['status'] = 'CARDNUMBER_INVALID';
        $response['message'] = 'Card number Invalid';
        break;
      case '9':// Invalid Card name//Giao dịch không thành công, Tên chủ thẻ không đúng
        $response['status'] = 'CARDNAME_INVALID';
        $response['message'] = 'Card name Invalid';
        break;
      case '10':// Expired Card// Giao dịch không thành công, Thẻ hết hạn/Thẻ bị khóa
        $response['status'] = 'CARD_EXPIRED';
        $response['message'] = 'Card Expired';
        break;
      case '11'://Card Not Registed Service(internet banking)//Giao dịch không thành công, Thẻ chưa đăng ký sử dụng dịch vụ
        $response['status'] = 'NOT_REGISTED';
        $response['message'] = 'Not Registed Service';
        break;
      case '12':// Invalid card date// Giao dịch không thành công, Ngày phát hành/Hết hạn không đúng
        $response['status'] = 'CARD_DATE_INVALID';
        $response['message'] = 'SUCCESS';
        break;
      case '13':// Exist Amount// Giao dịch không thành công, Vượt quá hạn mức thanh toán
        $response['status'] = 'AMOUNT_EXIST';
        $response['message'] = 'Amount Exist';
        break;
      case '21':// Insufficient fund// Giao dịch không thành công, Số tiền không đủ để thanh toán
        $response['status'] = 'INSUFFICIENT_FUND';
        $response['message'] = 'Insufficient funds';
        break;
      case '22':// Invalid Account// Giao dịch không thành công, Thông tin tài khoản không đúng
        $response['status'] = 'ACCOUNT_INVALID';
        $response['message'] = 'Account Invalid';
        break;
      case '23'://Account Lock//Giao dịch không thành công, Tài khoản bị khóa
        $response['status'] = 'ACCOUNT_LOCKED';
        $response['message'] = 'Account Locked';
        break;
      case '24'://Invalid Card Info//Giao dịch không thành công, Thông tin thẻ không đúng
        $response['status'] = 'INFO_INVALID';
        $response['message'] = 'Card Info Invalid';
        break;
      case '25'://Invalid OTP//Giao dịch không thành công, OTP không đúng.
        $response['status'] = 'OTP_INVALID';
        $response['message'] = 'OTP Invalid';
        break;
      case '253'://Transaction timeout//Giao dịch không thành công, Quá thời gian thanh toán
        $response['status'] = 'TRANSACTION_TIMEOUT';
        $response['message'] = 'Transaction timeout';
        break;
      case '99'://User cancel transaction//Giao dịch không thành công, Người sử dụng hủy giao dịch
        $response['status'] = 'CANCELLED';
        $response['message'] = 'User cancel transaction';
        break;
    }
    return $response;
  }

  public function internationalResult($code) {
    $response = [];
    switch ($code) {
      case '0'://Transaction is successful//Giao dịch thành công
        $response['status'] = 'SUCCESS';
        $response['message'] = 'Successful transaction.';
        break;
      case '2':
        //Bank Declined Transaction
        //Giao dịch không thành công. Ngân hàng phát hành từ chối cấp phép, do 1 trong những nguyên nhân sau:
        //Số dư không đủ thanh toán, Chưa đăng ký dịch vụ thanh toán trực tuyến, Ngày hết hạn
        $response['status'] = 'DECLINED';
        $response['message'] = 'Bank Declined Transaction.';
        break;
      case '1':
      case '3':
      case '9':
        //Issuer Bank declined the transaction.
        //Giao dịch không thành công , Cổng thanh toán không nhận được kết quả trả về từ ngân hàng phát hành thẻ.
        $response['status'] = 'ISSUER_DECLINED';
        $response['message'] = 'Issuer Bank declined the transaction.';
        break;
      case '4':// Your card is expired//Giao dịch không thành công. Thẻ hết hạn sử dụng
        $response['status'] = 'CARD_EXPIRED';
        $response['message'] = 'Card Expired';
        break;
      case '5':
        //Your credit account is insufficient funds
        //Giao dịch không thành công , Thẻ không đủ hạn mức hoặc tài khoản không đủ số dư thanh toán.
        $response['status'] = 'INSUFFICIENT_FUND';
        $response['message'] = 'Insufficient funds';
        break;
      case '6':// Error from Issuer Bank. Giao dịch không thành công, lỗi từ ngân hàng phát hành thẻ.
        $response['status'] = 'ISSUER_ERROR';
        $response['message'] = 'Error from Issuer Bank.';
        break;
      case '7':
        //Error when processing transaction
        //Giao dịch không thành công, lỗi phát sinh trong quá trình xử lý giao dịch
        $response['status'] = 'PROCESS_ERROR';
        $response['message'] = 'Error when processing transaction.';
        break;
      case '8':
        //Issuer Bank does not support E-commerce transaction
        //Giao dịch không thành công, Ngân hàng phát hành thẻ không hỗ trợ giao dịch Internet
        $response['status'] = 'ISSUER_NOT_SUPPORT';
        $response['message'] = 'Issuer Bank does not support E-commerce transaction.';
        break;
      case '99':// User cancel//Giao dịch thất bại. Người dùng hủy giao dịch
        $response['status'] = 'CANCELLED';
        $response['message'] = 'User cancel transaction';
        break;
      case 'B':
        //Cannot authenticated by 3D-Secure Program. Please contact Issuer Bank.
        //Giao dịch không thành công , không xác thực được 3DSecure. Liên hệ ngân hàng phát hành để được hỗ trợ.
        $response['status'] = 'NOT_AUTHENTICATION';
        $response['message'] = 'Cannot authenticated by 3D-Secure Program.';
        break;
      case 'E':
        //Wrong CSC entered or Issuer Bank declined the transaction. Please contact Issuer Bank.
        //Giao dịch không thành công. Bạn nhập sai CSC hoặc thẻ vượt quá hạn mức lần thanh toán
        $response['status'] = 'CSC_WRONG';
        $response['message'] = 'Wrong CSC entered or Issuer Bank declined the transaction. Please contact Issuer Bank';
        break;
      case 'F'://3D Secure Authentication Failed 
        $response['status'] = '3DSECURE_FAIL';
        $response['message'] = 'Wrong CSC entered or Issuer Bank declined the transaction. Please contact Issuer Bank';
        break;
      case 'Z'://Transaction was block by OFD//Giao dịch không thành công, bị chặn bởi hệ thống ODF
        $response['status'] = 'TRANSACTION_BLOCKED';
        $response['message'] = 'Transaction was block by OFD';
        break;
      default:
        //Transaction was failed.//Giao dịch không thành công
        $response['status'] = 'TRANSACTION_FAIL';
        $response['message'] = 'Transaction was failed.';
        break;
    }

    return $response;
  }

  private static function hashHmac($strHashData, $secureSecret) {
    return strtoupper(hash_hmac('SHA256', $strHashData, pack('H*', $secureSecret)));
  }

}