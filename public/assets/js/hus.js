function validateForm(formId) {
  var form = document.getElementById(formId);
  if (form.checkValidity() === false) {
    $("#"+formId).addClass('was-validated');
    $("#"+formId).find('input:not([disabled]):required').each(function () {
      if ($(this).val()) {
        $(this).siblings('.invalid-feedback').hide();
        $(this).siblings('.select-dropdown').css('border-color', '#ced4da');
      } else {
        $(this).siblings('.invalid-feedback').show();
        $(this).siblings('.select-dropdown').css('border-color', '#dc3545');
      }
    });
    return false;
  }
  $('.invalid-feedback').hide();

  var isErr = false;
  $("#"+formId).find('input.datepicker:not([disabled]):required').each(function () {
    if ($(this).val().length === 0) {
      $("#"+formId).addClass('was-validated');
      $(this).siblings('.invalid-feedback').show();
      $(this).css('border-color', '#dc3545');
      isErr = true;
    } else {
      $(this).siblings('.invalid-feedback').hide();
      $(this).css('border-color', '#ced4da');
    }
  });

  return !isErr;
}
//////////////////////// callAjax jQuery //////////////////////////////////////
/* Usage:

var dataSend = {id:'testID', loading:'loadingID', param_1:'paramValue_1', param_n:'paramValue_n'};
callAjax('Controller_Name', 'Action_Name', dataSend, testCallback);

function testCallback(result) {
	if (result !== false) {
        //Code here
	}
}

in which:
testID : the id of element we want to work.
loadingID : the id of loading icon we want to display.
paramValue_n: all parameters we want to post to server.
*/
function base64_decode(data) {
  return decodeURIComponent(atob(data).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

function fetchForm(formObj, getAll, upload) {
  if (upload === undefined) {
    upload = false;
  }

  if (getAll === undefined) {
    getAll = false;
  }

  let disabledFields = formObj.find('[disabled]');
  if (getAll) {
    disabledFields.prop('disabled', false);
  }

  let formData = {};
  if (upload === true) {
    formData = new FormData(formObj[0]);
  } else {
    formData = formObj.serializeJSON();
  }

  if (getAll) {
    disabledFields.prop('disabled', true);
  }

  return formData;
}

function callAjax(controller, method, args, callback) {
  var isLoading = false, dataType = 'json', isAsync = true,
    contentType = 'application/x-www-form-urlencoded; charset=UTF-8', processData = true;

  if (args !== null && args['silent'] === null) {
    isLoading = true;
    $("#loading").show();
  }

  if (args !== null && args['dataType'] !== null) {
    dataType = args['dataType'];
  }

  if (args !== null && args['isAsync'] !== null) {
    isAsync = args['isAsync'];
  }

  var data = args;
  if (args !== null && args['formData'] !== null) {
    var formData = args['formData'];
    delete args['formData'];

    if (args['upload'] !== null && args['upload']) {
      contentType = false;
      processData = false;
      data = formData;
    } else {
      data = $.extend({}, args, formData);
    }
  }

  var url = root_url + Array(controller, method).join("/");
  var urlArr = controller.split(":/");
  if (urlArr[0] === 'https' || urlArr[0] === 'http') {
    url = Array(controller, method).join("/");
  }

  var objectCall = $.ajax({
    type: "POST",
    timeout: 99999999999999,
    url: url,
    data: data,
    dataType: dataType,
    async: isAsync,
    contentType: contentType,
    processData: processData,
    success: function (msg) {
      if (args['is_append'] === null) {
        $("#" + args['id']).html('');
      }
      $("#loading").hide();

      if (msg === null) {
        if (isLoading) {
          $("#loading").hide();
          if ($("#" + args['id']).length > 0) {
            $("#" + args['id']).hide();
          }
        }
        return;
      }

      if (msg.result === "expired_session") {
        alert("Session expired.");
        location.reload();
        return;
      }

      if (msg.result === false && msg.messages) {
        showErrorBubble("", msg.messages);
      }

      eval(base64_decode(msg.html));

      if (callback !== null) {
        if (msg.result === undefined) {
          callback(msg, args['id']);
        } else {
          callback(msg.result, args['id']);
        }
      }
    },
    error: function (request, status, error) {
      if (request.statusText === 'abort' || request.statusText === 'error') return;
      if (error.name === 'NS_ERROR_NOT_AVAILABLE' || request.readyState === 0) {
        if (args['silent'] === null) {
          $('#infoText').html("Request is interrupted unexpectedly");
          setTimeout(function () {
            $('#loading').show();
          }, 3000);

        }
      } else {
        //default is display html return
        $('#' + args['id']).html(request.responseText);
      }
    }
  });
  return objectCall;
}

/////////////////////// End callAjax jQuery ///////////////////////////////////

// Show tooltip when input data is not valid
function showErrorBubble(control, error_msg, seconds) {
  var ctrl = false;
  if ($(control).length > 0) {
    ctrl = $(control);
  }
  var delay = seconds || 5;
  toastr.error(error_msg, '', {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "timeOut": delay*1000
  });

  if (ctrl !== false) {
    ctrl.focus();
  }

  return false;
}

// Show tooltip when saving data successfully
function showSuccessBubble(success_msg, seconds) {
  var delay = seconds || 5;
  toastr.success(success_msg, '', {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-top-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
    "timeOut": delay*1000
  });
  return false;
};
