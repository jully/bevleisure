$(document).ready(function () {
    $('#btnSearch').on('click', function () {
        if (!validateForm('searchForm')) {
            return false;
        }

        getRooms(1);
        return false;
    });
    
    $('#roomNum').on('change', function () {
        var roomNum = $(this).val();
        $('.roomInfo:not([class="hidden"])').addClass('hidden');
        $('.adtInfo').removeAttr('name');
        $('.chdInfo').removeAttr('name');
        $('.roomInfo.hidden').each(function (index) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
            if(index < parseInt(roomNum)){
                $(this).find('select.adtInfo').attr('name','rooms['+index+'][adt]');
                $(this).find('select.chdInfo').attr('name','rooms['+index+'][chd]');
                $(this).removeClass('hidden');
            }
        });
        return false;
    });
   
    
    $('#searchBooking').on('click', function () {
        callAjax('application', 'searchBooking', {
        }, searchBookingCallback);
    });
    
    $('#agreement').on('change', function () {
        if (!$(this).is(':checked')) {
            $('.btnPayment').removeClass('awe-btn-13');
            $('.btnPayment').removeAttr('id');
        } else {
            $('.btnPayment').addClass('awe-btn-13');
            $('.btnPayment').attr('id', 'btnPayment');
        }
    });
    
    $('#btnPayment').unbind().on('click', function () {
        if (!validateForm('guestForm')) {
            return false;
        }

        reservation();
        return false;
    });
    
    if ($('#listRooms').length > 0) {
        getRooms(1);
    }

    $("#country").select2({
        placeholder: "Please enter the country",
        minimumInputLength: 1,
        ajax: {
            url: '/get-country',
            dataType: 'json',
            delay: 300,
            cache: true,
            data: function (params) {
                return {
                    query: params.term,
                    page: params.page
                }
            },
            processResults: function (data, params) {
                var items = $.map(data.items, function (obj) {
                    //Indicate which fields are id and text
                    obj.id = obj.id || obj.code;
                    obj.text = obj.text || obj.name;

                    return obj;
                });

                params.page = params.page || 1;

                return {
                    results: items,
                    pagination: {
                        more: (params.page * 30) < data.total
                    }
                };
            },
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        templateSelection: function (data, container) {
            // Add custom attributes to the <option> tag for the selected option
            $(data.element).attr('data-phonecode', data.phone_code);
            return data.text;
        }
    });

});

function getRooms(page, sortField, sortType) {
  if (typeof sortField === 'undefined') {
    sortField = "rate";
  }
  if (typeof sortType === 'undefined') {
    sortType = "DESC";
  }

  var sort = {
    field: sortField,
    type: sortType
  };

  callAjax('application', 'getRooms', {
//    page: page,
    sort: sort,
    formData: fetchForm($('#searchForm'))
  }, getRoomsCallback);
}

function getRoomsCallback(result) {
  if (result !== false) {
  }
  return false;
}

function roomInfo(roomTypeCode){
    callAjax('application', 'getRoomInfo', {
        roomTypeCode : roomTypeCode
    }, roomInfoCallback);
}

function roomInfoCallback(result) {
  if (result !== false) {
    $('#commonDialog').modal('show');

    $('.close').on('click', function () {
        $('#commonDialog').modal('hide');
    });
  }
  return false;
}

function reservation(){
    callAjax('application', 'reservation', {
    formData: fetchForm($('#guestForm'))
    }, reservationCallback);
}

function reservationCallback(result) {
  if (result !== false) {
      window.location.replace(result);
  }
  return false;
}


function searchBookingCallback(result) {
  if (result !== false) {
      $('#commonDialog').modal('show');
      
        $('#btnSearchBooking').on('click', function () {
            if (!validateForm('searchBookingForm')) {
                return false;
            }
            
            //Retrive booking
            callAjax('application', 'retriveBooking', {
                formData: fetchForm($('#searchBookingForm'))
            }, retriveBookingCallback);
        });
      
       $('.close').on('click', function () {
            $('#commonDialog').modal('hide');
        });
  }
  return false;
}

function retriveBookingCallback(result) {
  if (result !== false) {
      window.open(root_url + 'my-booking?code='+result);
  }
  return false;
}
